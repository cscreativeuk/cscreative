<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
 * Some global route patterns
 */
Route::pattern('id', '[0-9]+');
Route::pattern('email', '[A-Za-z_0-9\-.]+@([a-z0-9]+\.)*[a-z0-9]+');
Route::pattern('hash', '[a-f0-9]{12,32}');

Route::get('/', function () {
    return View::make('home');
});

Route::post('/signup', 'SignupCaptureController@capture');

/*
 * Set up mailgun controller
 */
MailgunController::route();

/*
 * Set up auth controller
 */
AuthController::route();

/*
 * Set up blog controller
 */
BlogController::route();

/*
 * Set up landing controller
 */
LandingController::route();

Route::get('test', function()
{
    var_dump(App::make('markdown'));
});
