<?php

class AuthController extends BaseController
{
    /**
     * Set up the required routes for this controller
     */
    public static function route()
    {
        Route::group(array('prefix' => 'auth'), function()
        {
            Route::get('verify/{email}/{hash}', 'AuthController@verify');
        });
    }

    public function verify($email, $verify_key)
    {
        $user = User::where('email', $email)->firstOrFail();
        if ($user->verify($verify_key))
        {
            return View::make('message')
                ->withTitle('Verification Success')
                ->withContent('The email address ' . htmlentities($user->email) . ' was verified correctly');
        }
        else
        {
            return View::make('auth.verify.failure');
        }
    }

} 
