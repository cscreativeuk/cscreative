<?php

class BlogController extends BaseController
{
    /**
     * Set up all the routes required for this controller
     */
    public static function route()
    {
        Route::get('blog', 'BlogController@showIndex');
        Route::get('blog/{permalink}', 'BlogController@showPost');
        Route::bind('permalink', function($value, $route)
        {
            return Blog\Post::where('permalink', $value)->firstOrFail();
        });
    }

    public function showIndex()
    {
        $posts = Blog\Post::paginate(10);
        return View::make('blog')
            ->withPosts($posts);
    }

    public function showPost(Blog\Post $post)
    {
        return View::make('blogpost')
            ->withPost($post);
    }
} 
