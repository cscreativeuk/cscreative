<?php

/**
 * Handles Mailgun API callbacks
 */
class MailgunController extends \BaseController
{
    /**
     * Add the required routes to the route list
     */
    public static function route()
    {
        Route::group(array('prefix' => '_mailgun'), function() {
            Route::post('drop', 'MailgunController@drop');
            Route::get('deliver', 'MailgunController@deliver');
        });
    }

    /**
     * Callback for the _mailgun/drop route
     */
    public function drop()
    {
        echo 'Message failure accepted, thanks.';

        return;
    }

    /**
     * Callback for the _mailgun/deliver route
     */
    public function deliver()
    {
        echo 'Message delivery accepted, thanks.';

        return;
    }
}
