<?php

class LandingController extends BaseController
{

    /**
     * Set up required routes for this controller
     */
    public static function route()
    {
        Route::get('/landing/{name}', 'LandingController@showLanding');
    }

    /**
     * Show a specified landing page.
     *
     * @param $id
     */
    public function showLanding($id)
    {
        if (View::exists("landings.$id"))
        {
            return View::make("landings.$id");
        }
        else
        {
            App::abort(404);
        }
    }

}
