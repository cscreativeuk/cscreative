<?php

class SignupCaptureController extends BaseController
{

    /**
     * Capture a signup
     */
    public function capture()
    {
        $user = new User();
        $user->email = Input::get('email');
        $user->save();
        $user->sendVerificationEmail();
        return View::make('message')
            ->withTitle('Entry Confirmed')
            ->withContent('Thanks for entering! We\'ll be in touch shortly if you\'ve won.');
    }

} 
