<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();

        $user = User::create(array('email' => 'jackprice@outlook.com'));
        $user->sendVerificationEmail();
    }

} 
