<?php

namespace Blog;

use Illuminate\Database\Eloquent\Model as Eloquent;
use App;
use Cache;

/**
 * Represents a blog post
 *
 * @property-read $id
 * @property-read Carbon $created_at
 * @property-read Carbon $updated_at
 * @property $permalink
 * @property $title
 * @property $markdown
 */
class Post extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blog_posts';

    /**
     * The equivalent HTML for the described markdown
     */
    private $html = null;

    /**
     * Convert this post's markdown to HTML
     *
     * @return string
     */
    public function getHtml()
    {
        if ($this->html == null)
        {
            $this->html = Cache::rememberForever($this->getCacheKey(), function()
            {
                return App::make('markdown')->text($this->markdown);
            });
        }

        return $this->html;
    }

    /**
     * Get the cache key for this post
     *
     * @return string
     */
    public function getCacheKey()
    {
        return "blog.post.html.{$this->id}";
    }

    /**
     * Mutator for the markdown property
     *
     * @param $value
     */
    public function setMarkdownAttribute($value)
    {
        $this->attributes['markdown'] = $value;
        Cache::forget($this->getCacheKey());
    }
} 
