<?php

use Carbon\Carbon;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserTrait;
use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 *
 * @property-read $id
 * @property-read Carbon $created_at
 * @property-read Carbon $updated_at
 * @property $email
 * @property bool $verified
 * @property $verify_key
 */
class User extends Eloquent implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    /**
     * Send an email verification to the user
     *
     * @return $this
     */
    public function sendVerificationEmail()
    {
        $this->verify_key = md5(str_random(32));
        $this->save();
        Mail::send('emails.auth.verify', array('user' => &$this), function ($message) {
            $message->to($this->email)
                ->subject('Please verify your email address');
        });

        return $this;
    }

    /**
     * Verify the user with a verification key
     *
     * @return bool True if successful
     */
    public function verify($key)
    {
        if ($key == $this->verify_key) {
            $this->verified   = true;
            $this->verify_key = null;
            $this->save();
        }

        return $this->verified;
    }

}
