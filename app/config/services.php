<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun'   => array(
        'domain' => 'cscreative.uk',
        'secret' => 'key-77gpk646n8i94uozzj-itm4nidbcjgk8',
    ),

    'mailchimp' => array(
        'secret' => 'b6d13516f740b28c69f8934a4cfa14a9-us8'
    ),

    'bitbucket' => array(
        'domain' => '',
        'secret' => 'ltipvf2OX7n9r4LjpOcMZyIValjgSy0V'
    ),

    'mandrill'  => array(
        'secret' => '',
    ),

    'stripe'    => array(
        'model'  => 'User',
        'secret' => '',
    ),

);
