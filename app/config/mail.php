<?php

return array(

    'driver'  => 'mailgun',

    'from'    => array('address' => 'hello@cscreative.uk', 'name' => 'CS Creative'),

    'pretend' => false,

);
