<?php

use Illuminate\Console\Command;
use Illuminate\Foundation\Composer;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ConfigureCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'configure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Configure this application for deployment.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->line('Configuring application');

        $this->comment('Bringing application down...');
        SSH::run(array('cd /var/www/cscreative.uk', 'php artisan down'), function($line)
            {
                echo $line;
            });

        $this->comment('Updating composer...');
        SSH::run(array('composer self-update'), function($line)
        {
            echo $line;
        });

        $this->comment('Updating dependencies...');
        SSH::run(array('cd /var/www/cscreative.uk', 'composer update'), function($line)
        {
            echo $line;
        });

        $this->comment('Migrating...');
        SSH::run(array('cd /var/www/cscreative.uk', 'php artisan migrate'), function($line)
        {
            echo $line;
        });
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
