<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>CS Creative</title>
    <link rel="stylesheet" type="text/css" href="./static/styles/home.css" />
    <link rel="stylesheet" type="text/css" href="./static/styles/font-awesome.min.css" />
</head>
<body>
<div id="background"></div>
<div id="background-filter"></div>
<header>
    <div class="container">
        <div class="slogan">We love <span class="text-yellow">digital</span></div>
        <div class="slogan-subtext">We create <span class="text-white">beautiful things</span> with a personal touch</div>
    </div>
</header>
<nav>
    <div class="container"><span class="text-yellow">CS</span>Creative</div>
</nav>

<section id="about">
    <div class="container">
        <h1><span class="text-yellow">About</span> Us</h1>
        <div class="logo"><img src="./static/images/logo-128.png" width="128" height="128" alt=""/></div>
        <h2>We're a creative design company specialising in web design, e-commerce and analytics based in Chorley, Lancashire.</h2>
        <p>From our perspective, whether your company is large or small, or whether you have thousands of employees or you're a sole trader makes no difference to us. You'll still get the same personalised treatment. When you register with us, we'll assign you a designated point of contact with expertise in your field. Let us show you what we can do for you.</p>
    </div>
</section>

<section id="services">
    <div class="container">
        <h1>Our <span class="text-yellow">Services</span></h1>
        <h2>We offer a wide variety of services, all tailored to your individual needs.</h2>
        <p>From our perspective, whether your company is large or small, or whether you have thousands of employees or you're a sole trader makes no difference to us. You'll still get the same personalised treatment. When you register with us, we'll assign you a designated point of contact with expertise in your field. Let us show you what we can do for you.</p>
        <div class="col-box">
            <div class="col">
                <div class="padding">
                    <div class="package">
                        <h2 class="text-yellow">Web Design</h2>
                        <p>Let us design a custom website for you!</p>
                        <p>We can produce beautiful, responsive and cross-device friendly sites from a single page upwards.</p>
                        <p>Packages start from just £250, with <span class="text-yellow">free hosting</span> for a year.</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="padding">
                    <div class="package">
                        <h2 class="text-yellow">Analytics</h2>
                        <p>Do you know who your visitors are?<br />Do you know how to optimise your website to maximise its potential?</p>
                        <p>We can help you figure all of this out, and more, with our comprehensive analytics packages.</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="padding">
                    <div class="package">
                        <h2 class="text-yellow">E-Commerce</h2>
                        <p>We can provide powerful, lightening-fast and secure e-commerce packages to suit any need or size.</p>
                        <p>Get in touch to talk about what we can offer!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="contact">
    <div class="container">
        <h1>Contact Us</h1>
        <h2>We're a friendly bunch, so feel free to get in touch in any way.</h2>
        <div class="address">
            <div class="email">hello@cscreative.uk</div>
            <div class="phone">0800 123 4567</div>
        </div>
        <h2>Alternatively, fill out the contact form below and we'll get back to you as soon as possible!</h2>
    </div>
</section>

<script type="text/javascript" src="./static/scripts/home.js"></script>
@include('analytics')
</body>
</html>
