<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Win a website!</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('static/styles/home.css') }}" />
        <style>
            #background {
                position: fixed;
                top: 0px;
                left: 0px;
                right: 0px;
                bottom: 0px;
                background: url('{{ asset('static/images/background.jpg') }}');
                background-size: cover;
                z-index: -1000;
            }

            #background-filter {
                position: fixed;
                top: 0px;
                left: 0px;
                right: 0px;
                bottom: 0px;
                z-index: -999;
                background: url('{{ asset('static/images/pattern.png') }}');
                background-size: 2px 2px;
            }
        </style>
        <!-- Facebook Conversion Code for Competition Entry -->
        <script>(function() {
          var _fbq = window._fbq || (window._fbq = []);
          if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
          }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6017291813376', {'value':'0.00','currency':'GBP'}]);
        </script>
        <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6017291813376&amp;cd[value]=0.00&amp;cd[currency]=GBP&amp;noscript=1" /></noscript>
    </head>
    <body>
        <div id="background"></div>
        <div id="background-filter"></div>
        <header>
            <div class="container">
                <div class="slogan">Win a  <span class="text-yellow">website</span></div>
                <div class="slogan-subtext">Enter to win a <span class="text-white">custom designed</span> website and free hosting for a year.</div>
            </div>
        </header>
        <section id="about">
            <div class="container">
                <h1><span class="text-yellow">Enter</span> now</h1>
                <h2>Fill in your email address below to enter the competition now</h2>
                <p>From our perspective, whether your company is large or small, or whether you have thousands of employees or you're a sole trader makes no difference to us. You'll still get the same personalised treatment. When you register with us, we'll assign you a designated point of contact with expertise in your field. Let us show you what we can do for you.</p>
                <form action="{{ url('signup') }}" method="post">
                    <input name="email" placeholder="email" style="border-radius: 2px; background: #fff; border: none; height: 40px; width: 100%; text-align: center; font-size: 1em;" />
                </form>
            </div>
        </section>
        <section class="rules">
            <div class="container">
                <h1>Competition Rules</h1>
                <p>Entering this competition represents an agreement between you, or the company you represent (referred to as YOU) and CS Creative (the COMPANY).</p>
                <p>
                    1. Winners<br />
                    One winner will be chosen at random from all the unique valid entries submitted before the closing date of the competition.
                </p>
                <p>
                    2. Prize<br />
                    The winner selected as in section 1. will receive the following prize;<br />
                    A custom designed static website as described in the services section of the <a href="{{ url('') }}">CS Creative website</a>. Responsibility for meeting with representatives of the COMPANY to establish the details lies with the competition winner.<br />
                    One years hosting of said website, starting from the date of satisfactory agreement between the COMPANY and YOU on the design of the website. This is also subject to the terms and conditions of the static website described in the aforementioned services section of the CS Creative website.
                </p>
            </div>
        </section>

        @include('analytics')
    </body>
</html>
