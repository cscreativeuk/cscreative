@extends('common')

@section('title')
    CS Creative Blog
@overwrite

@section('content')
    <section class="page">
        <div class="container">
            <row>
                <div class="span-12">
                    <div class="title">Blog</div>
                        <p>Here you can find out what goes on in the background, or read articles about things our team is interested in.</p>
                    </div>
                </div>
            </row>
        </div>
    </section>

    @foreach($posts as $post)
        <section class="page">
            <div class="container">
                <row>
                    <div class="span-12">
                        <div class="title"><a href="{{ url('blog', $post->permalink) }}">{{{ $post->title }}}</a></div>
                        <p><small>Posted on {{{ $post->created_at }}}</small></p>
                        {{ $post->getHtml() }}
                    </div>
                </row>
            </div>
        </section>
    @endforeach
@stop
