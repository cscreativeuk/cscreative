<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charst="utf-8" />
        <title>@yield('title', 'CS Creative')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{ asset('static/css/base.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ asset('static/styles/font-awesome.min.css') }}" />
        @yield('styles')
    </head>
    <body>
        <div class="wrapper">
            <div id="mainnav-sticky-wrapper" class="sticky-wrapper is-sticky" style="height: 50px;"><nav id="mainnav" class="is-sticky" style="position: fixed; top: 0px;">
            			<div class="container">
            				<div class="row">
            					<div class="span5">
            						<a href="{{ url('') }}"></a>
            					</div>
            					<div class="span7">
            						<ul id="fluid-nav" class="fluid-navigation hidden-phone">
            							<li><a class="external" href="{{ url('') }}">Home</a></li>
            							<li><a class="external" href="{{ url('') }}#about">About Us</a></li>
            							<li><a class="external" href="{{ url('') }}#services">Services</a></li>
            							<li><a class="external" href="{{ url('') }}#contact">Contact Us</a></li>
            							<li class="current"><a class="external" href="{{ url('blog') }}">Blog</a></li>
            						</ul><select class="selectnav" id="selectnav1"><option value="">- Navigation -</option><option value="http://themes.tvda.eu/demos/goltsman/index.html">home</option><option value="http://themes.tvda.eu/demos/goltsman/index.html#about">about us</option><option value="http://themes.tvda.eu/demos/goltsman/index.html#team">team</option><option value="http://themes.tvda.eu/demos/goltsman/index.html#services">services</option><option value="http://themes.tvda.eu/demos/goltsman/index.html#portfolio">portfolio</option><option value="http://themes.tvda.eu/demos/goltsman/index.html#contact">contact us</option><option value="http://themes.tvda.eu/demos/goltsman/shortcodes.html" selected="">shortcodes</option></select>
            						<ul class="social-icons">
            							<li><a href="https://www.facebook.com/cscreativeuk"><i class="fa fa-facebook"></i></a></li>
            							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
            							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            						</ul>
            					</div>
            				</div>
            			</div>
            		</nav></div>
        </div>

        @yield('content')

        <section id="copyright">
            <div class="container">
                <div class="row">
                    <div class="span12">© 2014 <strong>CS Creative</strong>. </div>
                </div>
            </div>
        </section>

        <script type="text/javascript" src="{{ asset('static/scripts/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('static/scripts/bootstrap.min.js') }}"></script>
        @yield('scripts')
        @include('analytics')
    </body>
</html>
