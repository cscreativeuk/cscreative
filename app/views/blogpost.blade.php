@extends('common')

@section('title')
    CS Creative Blog
@overwrite

@section('content')
    <section class="page">
        <div class="container">
            <row>
                <div class="span-12">
                    <div class="title">{{{ $post->title }}}</div>
                    <p><small>Posted on {{{ $post->created_at }}}</small></p>
                    {{ $post->getHtml() }}
                </div>
            </row>
        </div>
    </section>
@stop
