<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
    </head>
    <body style="margin: 0; padding: 0; font-family: 'Montserrat', Arial, sans-serif;">
        <div style="width: 100%; background: #f9f9f9; color: #3e4349; padding-top: 10em; padding-bottom: 10em; line-height: 1.3;">
            <div style="width: 80%;
                        min-width: 400px;
                        max-width: 900px;
                        margin: 0 auto;">
                <div style="margin-left: 10px; margin-right: 10px;">
                <h1 style="text-transform: uppercase;
                           font-size: 3em;
                           padding: 0;
                           margin: 0;
                               margin-bottom: 2em;"><span style="color: #fec400;">Email</span> Verification</h1>
                    <h2 style="font-size: 1.5em;
                               text-transform: none;
                               padding: 0;
                               margin: 0;
                               margin-bottom: 2em;">Hi,</h2>
                    <p>You entered your email address at cscreative.uk recently. Please click the link below to verify this was you. If this was not you, you can safely ignore this email and you won't be contacted again.</p>
                    <p style="text-align: center;"><a href="{{ url('auth/verify', array('email' => $user->email, 'verify_key' => $user->verify_key)) }}">Verify Here</a></p>
                    <p>Thanks,</p>
                    <p>The Team</p>
                </div>
            </div>
        </div>
        <div style="width: 100%; background: #fec400; color: #f9f9f9; padding-top: 10em; padding-bottom: 10em; line-height: 1.3;">
            <div style="width: 80%;
                        min-width: 400px;
                        max-width: 900px;
                        margin: 0 auto;">
                <div style="margin-left: 10px; margin-right: 10px; text-align: center;">
                    <p>This email was sent for and on behalf of CS Creative. See our website for further details.</p>
                    <p>
                        <a href="{{ url('') }}?utm_source=signup&utm_medium=email&utm_content=logo&utm_campaign=verify&utm_user={{{ $user->email }}}">
                            <img src="{{ $message->embed(__DIR__ . '/../../../public/static/images/logo-128.png') }}" width="128" height="128" />
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
