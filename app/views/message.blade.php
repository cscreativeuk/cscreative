@extends('common')

@section('title')
    {{ $title }}
@overwrite

@section('content')
    <section class="page">
        <div class="container">
            <row>
                <div class="span-12">
                    <div class="title">{{ $title }}</div>
                        <p>{{ $content }}</p>
                    </div>
                </div>
            </row>
        </div>
    </section>
@stop
